EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 69 81
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega32U4-AU U?
U 1 1 5F68AE03
P 3750 4100
AR Path="/5F68AE03" Ref="U?"  Part="1" 
AR Path="/5F679651/5F68AE03" Ref="U?"  Part="1" 
AR Path="/5F529742/5F68AE03" Ref="U6901"  Part="1" 
F 0 "U6901" H 2900 2950 50  0000 C CNN
F 1 "ATmega32U4-AU" H 2900 2850 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 3750 4100 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf" H 3750 4100 50  0001 C CNN
	1    3750 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F68AE09
P 2800 2600
AR Path="/5F68AE09" Ref="R?"  Part="1" 
AR Path="/5F679651/5F68AE09" Ref="R?"  Part="1" 
AR Path="/5F529742/5F68AE09" Ref="R6901"  Part="1" 
F 0 "R6901" H 2870 2646 50  0000 L CNN
F 1 "10k" H 2870 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2730 2600 50  0001 C CNN
F 3 "~" H 2800 2600 50  0001 C CNN
	1    2800 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 2600 2650 2600
$Comp
L Device:Crystal_GND24 Y?
U 1 1 5F68AE11
P 2250 2900
AR Path="/5F68AE11" Ref="Y?"  Part="1" 
AR Path="/5F679651/5F68AE11" Ref="Y?"  Part="1" 
AR Path="/5F529742/5F68AE11" Ref="Y6901"  Part="1" 
F 0 "Y6901" V 2204 3031 50  0000 L CNN
F 1 "16MHz" V 2295 3031 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002CE-4Pin_3.2x2.5mm_HandSoldering" H 2250 2900 50  0001 C CNN
F 3 "~" H 2250 2900 50  0001 C CNN
	1    2250 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F68AE17
P 1550 2750
AR Path="/5F68AE17" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AE17" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AE17" Ref="C6901"  Part="1" 
F 0 "C6901" V 1298 2750 50  0000 C CNN
F 1 "22p" V 1389 2750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1588 2600 50  0001 C CNN
F 3 "~" H 1550 2750 50  0001 C CNN
	1    1550 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 2800 3000 2800
Wire Wire Line
	3000 2800 3000 2750
Wire Wire Line
	3000 3000 3000 3050
Wire Wire Line
	3000 3000 3150 3000
$Comp
L Device:C C?
U 1 1 5F68AE21
P 1550 3050
AR Path="/5F68AE21" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AE21" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AE21" Ref="C6902"  Part="1" 
F 0 "C6902" V 1700 3050 50  0000 C CNN
F 1 "22p" V 1800 3050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1588 2900 50  0001 C CNN
F 3 "~" H 1550 3050 50  0001 C CNN
	1    1550 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	1400 2750 1300 2750
Wire Wire Line
	1300 3050 1400 3050
Wire Wire Line
	3650 2050 3650 2300
Wire Wire Line
	3750 5900 3700 5900
$Comp
L Device:C C?
U 1 1 5F68AE3F
P 1550 3500
AR Path="/5F68AE3F" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AE3F" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AE3F" Ref="C6903"  Part="1" 
F 0 "C6903" V 1298 3500 50  0000 C CNN
F 1 "100n" V 1389 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1588 3350 50  0001 C CNN
F 3 "~" H 1550 3500 50  0001 C CNN
	1    1550 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 3600 3150 3600
Wire Wire Line
	3000 3700 3150 3700
$Comp
L Device:C C?
U 1 1 5F68AE49
P 1550 4000
AR Path="/5F68AE49" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AE49" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AE49" Ref="C6904"  Part="1" 
F 0 "C6904" V 1298 4000 50  0000 C CNN
F 1 "1u" V 1389 4000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1588 3850 50  0001 C CNN
F 3 "~" H 1550 4000 50  0001 C CNN
	1    1550 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 3400 3150 3400
$Comp
L Device:C C?
U 1 1 5F68AE51
P 3800 1450
AR Path="/5F68AE51" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AE51" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AE51" Ref="C6906"  Part="1" 
F 0 "C6906" V 3548 1450 50  0000 C CNN
F 1 "100n" V 3639 1450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3838 1300 50  0001 C CNN
F 3 "~" H 3800 1450 50  0001 C CNN
	1    3800 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 1450 3500 1450
Wire Wire Line
	3150 3200 2450 3200
Wire Wire Line
	2450 3200 2450 3500
Wire Wire Line
	2450 3500 1700 3500
Wire Wire Line
	3150 3900 2450 3900
Wire Wire Line
	2450 3900 2450 4000
Wire Wire Line
	2450 4000 1700 4000
Wire Wire Line
	1400 4000 1300 4000
Wire Wire Line
	1300 3500 1400 3500
Wire Wire Line
	4100 1450 3950 1450
Connection ~ 1300 3500
Wire Wire Line
	1300 3500 1300 3350
Connection ~ 1300 3050
$Comp
L Device:LED D?
U 1 1 5F68AE65
P 5400 4300
AR Path="/5F68AE65" Ref="D?"  Part="1" 
AR Path="/5F679651/5F68AE65" Ref="D?"  Part="1" 
AR Path="/5F529742/5F68AE65" Ref="D6901"  Part="1" 
F 0 "D6901" H 5400 4050 50  0000 C CNN
F 1 "RX" H 5400 4150 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5400 4300 50  0001 C CNN
F 3 "~" H 5400 4300 50  0001 C CNN
	1    5400 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F68AE6B
P 4950 4300
AR Path="/5F68AE6B" Ref="R?"  Part="1" 
AR Path="/5F679651/5F68AE6B" Ref="R?"  Part="1" 
AR Path="/5F529742/5F68AE6B" Ref="R6904"  Part="1" 
F 0 "R6904" H 5020 4346 50  0000 L CNN
F 1 "330" H 5020 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4880 4300 50  0001 C CNN
F 3 "~" H 4950 4300 50  0001 C CNN
	1    4950 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 4300 5100 4300
Wire Wire Line
	4350 4300 4800 4300
Wire Wire Line
	5550 4300 5700 4300
$Comp
L Device:LED D?
U 1 1 5F68AE75
P 5500 2600
AR Path="/5F68AE75" Ref="D?"  Part="1" 
AR Path="/5F679651/5F68AE75" Ref="D?"  Part="1" 
AR Path="/5F529742/5F68AE75" Ref="D6902"  Part="1" 
F 0 "D6902" H 5500 2350 50  0000 C CNN
F 1 "TX" H 5500 2450 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5500 2600 50  0001 C CNN
F 3 "~" H 5500 2600 50  0001 C CNN
	1    5500 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F68AE7B
P 5050 2600
AR Path="/5F68AE7B" Ref="R?"  Part="1" 
AR Path="/5F679651/5F68AE7B" Ref="R?"  Part="1" 
AR Path="/5F529742/5F68AE7B" Ref="R6905"  Part="1" 
F 0 "R6905" H 5120 2646 50  0000 L CNN
F 1 "330" H 5120 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4980 2600 50  0001 C CNN
F 3 "~" H 5050 2600 50  0001 C CNN
	1    5050 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 2600 5200 2600
Wire Wire Line
	5650 2600 5800 2600
Wire Wire Line
	2950 2600 3050 2600
Wire Wire Line
	3700 6000 3700 5900
Connection ~ 3700 5900
Wire Wire Line
	3700 5900 3650 5900
Wire Wire Line
	4350 3800 4450 3800
Wire Wire Line
	4350 3900 4450 3900
Wire Wire Line
	4350 4200 4450 4200
Wire Wire Line
	4350 4500 4450 4500
Wire Wire Line
	4450 4800 4350 4800
Wire Wire Line
	4450 3000 4350 3000
Wire Wire Line
	4350 3100 4450 3100
Wire Wire Line
	4350 3500 4450 3500
Wire Wire Line
	4450 3200 4350 3200
Wire Wire Line
	4450 2700 4350 2700
Wire Wire Line
	4450 2800 4350 2800
Wire Wire Line
	4350 3300 4450 3300
Wire Wire Line
	4350 3600 4450 3600
Wire Wire Line
	4350 4400 4450 4400
$Comp
L Device:R R?
U 1 1 5F68AEB1
P 2850 3700
AR Path="/5F68AEB1" Ref="R?"  Part="1" 
AR Path="/5F679651/5F68AEB1" Ref="R?"  Part="1" 
AR Path="/5F529742/5F68AEB1" Ref="R6903"  Part="1" 
F 0 "R6903" H 2920 3746 50  0000 L CNN
F 1 "22" H 2920 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2780 3700 50  0001 C CNN
F 3 "~" H 2850 3700 50  0001 C CNN
	1    2850 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F68AEB7
P 2850 3600
AR Path="/5F68AEB7" Ref="R?"  Part="1" 
AR Path="/5F679651/5F68AEB7" Ref="R?"  Part="1" 
AR Path="/5F529742/5F68AEB7" Ref="R6902"  Part="1" 
F 0 "R6902" H 2920 3646 50  0000 L CNN
F 1 "22" H 2920 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2780 3600 50  0001 C CNN
F 3 "~" H 2850 3600 50  0001 C CNN
	1    2850 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 3700 2550 3700
Wire Wire Line
	2700 3600 2550 3600
Wire Wire Line
	4350 2600 4900 2600
Wire Wire Line
	4350 2900 4450 2900
Wire Wire Line
	2350 2900 2050 2900
Wire Wire Line
	3050 2450 3050 2600
Connection ~ 3050 2600
Wire Wire Line
	3050 2600 3150 2600
Wire Wire Line
	4350 4000 4450 4000
Wire Wire Line
	4350 4100 4450 4100
Wire Wire Line
	1700 2750 2250 2750
Wire Wire Line
	1700 3050 2250 3050
Wire Wire Line
	3650 2300 3750 2300
Connection ~ 3750 2300
Wire Wire Line
	3750 2300 3850 2300
Connection ~ 3650 2300
Connection ~ 2250 3050
Wire Wire Line
	2250 3050 3000 3050
Connection ~ 2250 2750
Wire Wire Line
	2250 2750 3000 2750
Wire Wire Line
	1300 2750 1300 3050
$Comp
L Device:C C?
U 1 1 5F68AEEC
P 4900 1450
AR Path="/5F68AEEC" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AEEC" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AEEC" Ref="C6907"  Part="1" 
F 0 "C6907" V 4648 1450 50  0000 C CNN
F 1 "100n" V 4739 1450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4938 1300 50  0001 C CNN
F 3 "~" H 4900 1450 50  0001 C CNN
	1    4900 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 1450 4600 1450
Wire Wire Line
	5200 1450 5050 1450
$Comp
L Device:C C?
U 1 1 5F68AEFB
P 2700 1450
AR Path="/5F68AEFB" Ref="C?"  Part="1" 
AR Path="/5F679651/5F68AEFB" Ref="C?"  Part="1" 
AR Path="/5F529742/5F68AEFB" Ref="C6905"  Part="1" 
F 0 "C6905" V 2448 1450 50  0000 C CNN
F 1 "100n" V 2539 1450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2738 1300 50  0001 C CNN
F 3 "~" H 2700 1450 50  0001 C CNN
	1    2700 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 1450 2400 1450
Wire Wire Line
	3000 1450 2850 1450
Text Label 2400 1450 2    50   ~ 0
+5
Text Label 3500 1450 2    50   ~ 0
+5
Text Label 4600 1450 2    50   ~ 0
+5
Text Label 3650 2050 2    50   ~ 0
+5
Text Label 2550 2600 2    50   ~ 0
+5
Text Label 3000 3400 2    50   ~ 0
+5
Text Label 5800 2600 0    50   ~ 0
+5
Text Label 5700 4300 0    50   ~ 0
+5
Text Label 3700 6000 3    50   ~ 0
GND
Text Label 950  3350 2    50   ~ 0
GND
Wire Wire Line
	1300 3500 1300 4000
Wire Wire Line
	950  3350 1300 3350
Connection ~ 1300 3350
Wire Wire Line
	1300 3350 1300 3050
Text Label 1950 2900 2    50   ~ 0
GND
Wire Wire Line
	1950 2900 2050 2900
Text Label 2650 2900 0    50   ~ 0
GND
Wire Wire Line
	2650 2900 2450 2900
Connection ~ 2050 2900
Text Label 4450 4700 0    50   ~ 0
GND
Wire Wire Line
	4350 4700 4450 4700
Text Label 3000 1450 0    50   ~ 0
GND
Text Label 4100 1450 0    50   ~ 0
GND
Text Label 5200 1450 0    50   ~ 0
GND
Text Label 2550 3600 2    50   ~ 0
D+
Text Label 2550 3700 2    50   ~ 0
D-
Text Label 3050 2450 1    50   ~ 0
RESET
Text Label 4450 2700 0    50   ~ 0
SCK
Text Label 4450 2800 0    50   ~ 0
MOSI
Text Label 4450 2900 0    50   ~ 0
MISO
Text Label 4450 3000 0    50   ~ 0
D8
Text Label 4450 3100 0    50   ~ 0
D9-CLK
Text Label 4450 3200 0    50   ~ 0
D10
Text Label 4450 3300 0    50   ~ 0
D11
Text Label 4450 3500 0    50   ~ 0
D5
Text Label 4450 3600 0    50   ~ 0
D13
Text Label 4450 3800 0    50   ~ 0
D3-SCL
Text Label 4450 3900 0    50   ~ 0
D2_SDA
Text Label 4450 4000 0    50   ~ 0
D0-RX
Text Label 4450 4100 0    50   ~ 0
D1-TX
Text Label 4450 4200 0    50   ~ 0
D4
Text Label 4450 4400 0    50   ~ 0
D12-CNT
Text Label 4450 4500 0    50   ~ 0
D6
Text Label 4450 4800 0    50   ~ 0
D7
Text Label 8000 1950 2    50   ~ 0
+5
Text Label 8050 2050 2    50   ~ 0
GND
Text Label 8050 2550 2    50   ~ 0
RESET
Text Label 8050 2250 2    50   ~ 0
D+
Text Label 8050 2350 2    50   ~ 0
D-
Wire Wire Line
	8050 2850 8400 2850
Wire Wire Line
	8050 2750 8400 2750
Wire Wire Line
	8400 2650 8050 2650
Text Label 8050 2850 2    50   ~ 0
SCK
Text Label 8050 2750 2    50   ~ 0
MOSI
Text Label 8050 2650 2    50   ~ 0
MISO
Wire Wire Line
	8050 3300 8400 3300
Wire Wire Line
	8400 3200 8050 3200
Wire Wire Line
	8050 3100 8400 3100
Wire Wire Line
	8400 3000 8050 3000
Text Label 8050 3300 2    50   ~ 0
D8
Text Label 8050 3200 2    50   ~ 0
D9-CLK
Text Label 8050 3100 2    50   ~ 0
D10
Text Label 8050 3000 2    50   ~ 0
D11
Wire Wire Line
	8400 3500 8050 3500
Wire Wire Line
	8400 3400 8050 3400
Text Label 8050 3500 2    50   ~ 0
D5
Text Label 8050 3400 2    50   ~ 0
D13
Wire Wire Line
	8400 4000 8050 4000
Wire Wire Line
	8400 3900 8050 3900
Wire Wire Line
	8400 3600 8050 3600
Wire Wire Line
	8400 3800 8050 3800
Wire Wire Line
	8400 3700 8050 3700
Text Label 8050 4000 2    50   ~ 0
D3-SCL
Text Label 8050 3900 2    50   ~ 0
D2_SDA
Text Label 8050 3800 2    50   ~ 0
D0-RX
Text Label 8050 3700 2    50   ~ 0
D1-TX
Text Label 8050 3600 2    50   ~ 0
D4
Wire Wire Line
	8400 4100 8050 4100
Wire Wire Line
	8400 4200 8050 4200
Text Label 8050 4200 2    50   ~ 0
D12-CNT
Text Label 8050 4100 2    50   ~ 0
D6
Wire Wire Line
	8050 4300 8400 4300
Text Label 8050 4300 2    50   ~ 0
D7
Text Label 4450 5500 0    50   ~ 0
A0
Text Label 4450 5400 0    50   ~ 0
A1-D19
Text Label 4450 5300 0    50   ~ 0
A2-D20
Text Label 4450 5200 0    50   ~ 0
A3-D21
Text Label 4450 5100 0    50   ~ 0
A4
Text Label 4450 5000 0    50   ~ 0
A5
Wire Wire Line
	4450 5000 4350 5000
Wire Wire Line
	4450 5100 4350 5100
Wire Wire Line
	4450 5500 4350 5500
Wire Wire Line
	4350 5400 4450 5400
Wire Wire Line
	4350 5300 4450 5300
Wire Wire Line
	4350 5200 4450 5200
Text Label 8050 4400 2    50   ~ 0
A0
Text Label 8050 4500 2    50   ~ 0
A1-D19
Text Label 8050 4600 2    50   ~ 0
A2-D20
Text Label 8050 4700 2    50   ~ 0
A3-D21
Text Label 8050 4800 2    50   ~ 0
A4
Text Label 8050 4900 2    50   ~ 0
A5
Wire Wire Line
	8050 4900 8400 4900
Wire Wire Line
	8050 4800 8400 4800
Wire Wire Line
	8050 4400 8400 4400
Wire Wire Line
	8400 4500 8050 4500
Wire Wire Line
	8400 4600 8050 4600
Wire Wire Line
	8400 4700 8050 4700
Text HLabel 8400 1950 2    50   Input ~ 0
+5
Text HLabel 8400 2050 2    50   Input ~ 0
GND
Text HLabel 8400 2250 2    50   Input ~ 0
D+
Text HLabel 8400 2350 2    50   Input ~ 0
D-
Text HLabel 8400 2550 2    50   Input ~ 0
RESET
Text HLabel 8400 2650 2    50   Input ~ 0
MISO
Text HLabel 8400 2750 2    50   Input ~ 0
MOSI
Text HLabel 8400 2850 2    50   Input ~ 0
SCK
Text HLabel 8400 3000 2    50   Input ~ 0
D11
Text HLabel 8400 3100 2    50   Input ~ 0
D10
Text HLabel 8400 3200 2    50   Input ~ 0
D9-CLK
Text HLabel 8400 3300 2    50   Input ~ 0
D8
Text HLabel 8400 3400 2    50   Input ~ 0
D13
Text HLabel 8400 3500 2    50   Input ~ 0
D5
Text HLabel 8400 3600 2    50   Input ~ 0
D4
Text HLabel 8400 3700 2    50   Input ~ 0
D1-TX
Text HLabel 8400 3800 2    50   Input ~ 0
D0-RX
Text HLabel 8400 3900 2    50   Input ~ 0
D2-SDA
Text HLabel 8400 4000 2    50   Input ~ 0
D3-SCL
Text HLabel 8400 4100 2    50   Input ~ 0
D6
Text HLabel 8400 4200 2    50   Input ~ 0
D12-CNT
Text HLabel 8400 4300 2    50   Input ~ 0
D7
Text HLabel 8400 4400 2    50   Input ~ 0
A0
Text HLabel 8400 4500 2    50   Input ~ 0
A1-D19
Text HLabel 8400 4600 2    50   Input ~ 0
A2-D20
Text HLabel 8400 4700 2    50   Input ~ 0
A3-D21
Text HLabel 8400 4800 2    50   Input ~ 0
A4
Text HLabel 8400 4900 2    50   Input ~ 0
A5
Wire Wire Line
	8000 1950 8400 1950
Wire Wire Line
	8050 2050 8400 2050
Wire Wire Line
	8050 2250 8400 2250
Wire Wire Line
	8050 2350 8400 2350
Wire Wire Line
	8050 2550 8400 2550
$Comp
L Connector_Generic:Conn_01x06 PROG?
U 1 1 5F56592D
P 8450 5650
AR Path="/5F52F5D8/5F56592D" Ref="PROG?"  Part="1" 
AR Path="/5F56592D" Ref="PROG?"  Part="1" 
AR Path="/5F529742/5F56592D" Ref="PROG6901"  Part="1" 
F 0 "PROG6901" H 8530 5642 50  0000 L CNN
F 1 "Conn_01x06" H 8530 5551 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x06_P1.00mm_Vertical" H 8450 5650 50  0001 C CNN
F 3 "~" H 8450 5650 50  0001 C CNN
	1    8450 5650
	1    0    0    -1  
$EndComp
Text Label 7900 5850 2    50   ~ 0
RESET
Wire Wire Line
	7900 5550 8250 5550
Wire Wire Line
	7900 5650 8250 5650
Wire Wire Line
	8250 5750 7900 5750
Text Label 7900 5550 2    50   ~ 0
SCK
Text Label 7900 5650 2    50   ~ 0
MOSI
Text Label 7900 5750 2    50   ~ 0
MISO
Wire Wire Line
	7900 5850 8250 5850
Text Label 7900 5950 2    50   ~ 0
+5
Text Label 7900 5450 2    50   ~ 0
GND
Wire Wire Line
	7900 5450 8250 5450
Wire Wire Line
	7900 5950 8250 5950
$EndSCHEMATC
